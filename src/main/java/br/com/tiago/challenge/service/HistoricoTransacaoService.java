package br.com.tiago.challenge.service;

import br.com.tiago.challenge.entity.HistoricoTransacao;
import br.com.tiago.challenge.repository.HistoricoTransacaoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;

@Service
public class HistoricoTransacaoService {
    @Autowired
    private HistoricoTransacaoRepository historicoTransacaoRepository;

    public void saveHistorico(LocalDateTime dataTransacao, LocalDateTime dataImportacao) {
        HistoricoTransacao historicoTransacao = new HistoricoTransacao(dataTransacao, dataImportacao);
        this.historicoTransacaoRepository.save(historicoTransacao);
    }

    public List<HistoricoTransacao> findAll(Sort dataTransacao) {
        return this.historicoTransacaoRepository.findAll(dataTransacao);
    }
}
