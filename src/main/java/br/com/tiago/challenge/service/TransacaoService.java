package br.com.tiago.challenge.service;

import br.com.tiago.challenge.repository.TransacaoRepository;
import br.com.tiago.challenge.entity.Transacao;
import br.com.tiago.challenge.enums.ResultadoLeitura;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.time.Clock;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Service
public class TransacaoService {

    @Autowired
    private TransacaoRepository transacaoRepository;

    @Autowired
    private HistoricoTransacaoService historicoTransacaoService;

    public ResultadoLeitura readCsv(MultipartFile file) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(file.getInputStream()));
        String line = reader.readLine();
        LocalDateTime dataCarga = null;
        List<Transacao> transacaoList = new ArrayList<>();

        if(line.isEmpty()) {
            reader.close();
            return ResultadoLeitura.ARQUIVO_VAZIO;
        }

        while (Objects.nonNull(line)){
            String[] dadosTransacao = line.split(",");
            if(!this.verificaValorNulo(dadosTransacao)){
                Transacao transacao = this.converteParaTransacao(dadosTransacao);
                if(Objects.isNull(dataCarga)){
                    dataCarga = transacao.getDataTransacao();
                }
                List<Transacao> datasImportadas = this.transacaoRepository.findByDataTransacao(dataCarga);
                if(!datasImportadas.isEmpty()){
                    return ResultadoLeitura.DADOS_JA_IMPORTADOS;
                }
                if(this.verificaDiaTransacao(dataCarga, transacao.getDataTransacao())){
                    transacaoList.add(transacao);
                }
            }

            line = reader.readLine();
        }

        reader.close();

        this.transacaoRepository.saveAll(transacaoList);

        this.historicoTransacaoService.saveHistorico(dataCarga, LocalDateTime.now(Clock.systemDefaultZone()));

        return ResultadoLeitura.OK;
    }

    private boolean verificaDiaTransacao(LocalDateTime dataCarga, LocalDateTime dataTransacao) {

        boolean validador =
                dataTransacao.getYear() == dataCarga.getYear() &&
                dataTransacao.getMonth() == dataCarga.getMonth() &&
                dataTransacao.getDayOfMonth() == dataCarga.getDayOfMonth() ? true : false;
        return validador;
    }

    private Transacao converteParaTransacao(String[] dadosTransacao) {
        Transacao transacao = new Transacao();
        transacao.setBancoOrigem(dadosTransacao[0]);
        transacao.setAgenciaOrigem(Integer.parseInt(dadosTransacao[1]));
        transacao.setContaOrigem(dadosTransacao[2]);
        transacao.setBancoDestino(dadosTransacao[3]);
        transacao.setAgenciaDestino(Integer.parseInt(dadosTransacao[4]));
        transacao.setContaDestino(dadosTransacao[5]);
        transacao.setValorTransacao(new BigDecimal(dadosTransacao[6]));
        transacao.setDataTransacao(LocalDateTime.parse(dadosTransacao[7], DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss")));
        return transacao;
    }

    private boolean verificaValorNulo(String[] dadosTransacao) {

        for (String string: dadosTransacao) {
            if(string.isEmpty()){
                return true;
            }
        }
        return false;
    }


}
