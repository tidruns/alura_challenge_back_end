package br.com.tiago.challenge.repository;

import br.com.tiago.challenge.entity.HistoricoTransacao;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface HistoricoTransacaoRepository extends JpaRepository<HistoricoTransacao, Long> {
}
