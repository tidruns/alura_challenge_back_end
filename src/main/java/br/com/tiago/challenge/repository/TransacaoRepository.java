package br.com.tiago.challenge.repository;

import br.com.tiago.challenge.entity.Transacao;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.List;

@Repository
public interface TransacaoRepository extends JpaRepository<Transacao, Long> {
    List<Transacao> findByDataTransacao(LocalDateTime dataCarga);
}
