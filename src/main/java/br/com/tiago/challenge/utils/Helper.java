package br.com.tiago.challenge.utils;

public abstract class Helper {

    private static final long  MEGABYTE = 1024L * 1024L;

    public static String convertInMB(long size) {
        return String.valueOf(size / MEGABYTE);
    }
}
