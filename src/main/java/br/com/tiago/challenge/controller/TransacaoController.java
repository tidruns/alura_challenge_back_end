package br.com.tiago.challenge.controller;

import br.com.tiago.challenge.enums.ResultadoLeitura;
import br.com.tiago.challenge.service.HistoricoTransacaoService;
import br.com.tiago.challenge.service.TransacaoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.io.IOException;
import java.util.Objects;

@Controller
public class TransacaoController {

    @Autowired
    private TransacaoService transacaoService;

    @Autowired
    private HistoricoTransacaoService historicoTransacaoService;

    @GetMapping("/")
    public String form(){
        return "form";
    }

    @GetMapping("/listTransacao")
    public String listTransacao(Model model){
        model.addAttribute("historicoTransacao", this.historicoTransacaoService.findAll(Sort.by(Sort.Direction.DESC, "dataTransacao")));
        return "historicoTransacao";
    }

    @PostMapping("/upload")
    public String uploadFile(MultipartFile file, RedirectAttributes attributes) throws IOException, IllegalAccessException {

        if (Objects.isNull(file) || file.getOriginalFilename().isEmpty()) {
            attributes.addFlashAttribute("message", "Por favor selecione um arquivo para o upload.");
            return "redirect:/";
        }

        ResultadoLeitura resultadoLeitura = this.transacaoService.readCsv(file);

        if(resultadoLeitura == ResultadoLeitura.ARQUIVO_VAZIO){
            attributes.addFlashAttribute("message", "O arquivo envidado não possui transações.");
            return "redirect:/";
        }

        if(resultadoLeitura == ResultadoLeitura.DADOS_JA_IMPORTADOS){
            attributes.addFlashAttribute("message", "As transações do dia enviadas, já constá no sistema!");
            return "redirect:/";
        }

        attributes.addFlashAttribute("message", "Você adicionou com sucesso o arquivo " + file.getOriginalFilename() + '!');
        return "redirect:/listTransacao";
    }

}
