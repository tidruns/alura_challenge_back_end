package br.com.tiago.challenge.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Transacao {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String bancoOrigem;
    private Integer agenciaOrigem;
    private String contaOrigem;
    private String bancoDestino;
    private Integer agenciaDestino;
    private String contaDestino;
    private BigDecimal valorTransacao;
    private LocalDateTime dataTransacao;

}
