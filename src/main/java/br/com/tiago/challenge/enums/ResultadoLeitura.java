package br.com.tiago.challenge.enums;

public enum ResultadoLeitura {
    ARQUIVO_VAZIO,
    OK,
    DADOS_JA_IMPORTADOS;
}
